// Initialize Firebase (ADD YOUR OWN DATA)
let result = document.querySelector(".result");
let person = window.prompt("Please enter your name");
let sendSound = document.getElementById("sendSound")
var config = {
  apiKey: "AIzaSyCvI8K377-npqCrlcPt62vIfElFr4JJ-Lg",
  authDomain: "contactform-ae910.firebaseapp.com",
  databaseURL: "https://contactform-ae910.firebaseio.com",
  projectId: "contactform-ae910",
  storageBucket: "contactform-ae910.appspot.com",
  messagingSenderId: "837474833267",
  appId: "1:837474833267:web:454e58354e3654996eab06"
};

firebase.initializeApp(config);
database = firebase.database();
var ref = database.ref('messages');

// Reference messages collection
var messagesRef = firebase.database().ref('messages');

// Listen for form submit
document.getElementById('contactForm').addEventListener('submit', submitForm);



ref.on("value",function(data){
    var allData = document.querySelectorAll('.oldMarkerTwit')
  for (let i = 0; i < allData.length; i++) {
      allData[i].remove()
    // console.log(allData[i]);
    
  }
  //console.log(data.val());
  var data = data.val()
  var keys = Object.keys(data)
  for (let i = 0; i < keys.length; i++) {
    const article = document.createElement("article");
    article.setAttribute("class","oldMarkerTwit nav-link twit-container")
    const element = keys[i];
    grabDataName = data[element].name
    grabMessage = data[element].message
    grabDate = data[element].date
    grabTime = data[element].time
    article.innerHTML = `<h3>${grabDataName}</h3> 
    <h5>${grabMessage}</h5>
    <p>${grabDate}</p>
    <p>${grabTime}</p>
    `
     result.appendChild(article)
     sendSound.currentTime = 0;
     sendSound.play();
  }
  })
// Submit form
function submitForm(e){
  e.preventDefault();
  
  // Get values
//   var name = getInputVal('name');
var name = person
  var message = getInputVal('message');
  const date = new Date().toLocaleDateString();
  const time = new Date().toLocaleTimeString();
  // Save message
  saveMessage(name,message,date,time);
  // Clear form
  document.getElementById('contactForm').reset();
}

// Function to get get form values
function getInputVal(id){
  return document.getElementById(id).value;
}

// Save message to firebase
function saveMessage(name, message,date,time){
  var newMessageRef = messagesRef.push();
  newMessageRef.set({
    name: name,
    message:message,
    date:date,
    time:time
  });
}